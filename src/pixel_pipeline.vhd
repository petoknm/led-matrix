library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pixel_pipeline is
  port (
    ram_addr: in std_logic_vector(10 downto 0);
    ram_data: in std_logic_vector(15 downto 0);
    ram_we: in std_logic;
    ram_clk: in std_logic;

    pixel_clk: in std_logic;
    pixel_addr: in std_logic_vector(10 downto 0);
    pixel_pwm: in std_logic_vector(2 downto 0);

    rgb: out std_logic_vector(2 downto 0)
  );
end pixel_pipeline;

architecture rtl of pixel_pipeline is
  signal ram_out: std_logic_vector(15 downto 0);
begin

  output_register: process(pixel_clk)
  begin
    if rising_edge(pixel_clk) then
      rgb <= "000";
      if unsigned(ram_out(7 downto 5)) > unsigned(pixel_pwm) then
        rgb(0) <= '1';
      end if;
      if unsigned(ram_out(4 downto 2)) > unsigned(pixel_pwm) then
        rgb(1) <= '1';
      end if;
      if unsigned(ram_out(1 downto 0) & "0") > unsigned(pixel_pwm) then
        rgb(2) <= '1';
      end if;
    end if;
  end process;

  ram: entity work.ram port map (
    wrclock => ram_clk,
    rdclock => pixel_clk,
    data => ram_data,
    rdaddress => pixel_addr,
    wraddress => ram_addr,
    wren => ram_we,
    q => ram_out
  );

end rtl;
