library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity frame_writer is
  port (
    clk: in std_logic;

    ram_addr: out std_logic_vector(11 downto 0);
    ram_data: out std_logic_vector(15 downto 0);
    ram_clk: out std_logic;
    ram_we: out std_logic;

    spi_data: in std_logic_vector(15 downto 0);
    spi_data_ready: in std_logic;
    rst: in std_logic
  );
end frame_writer;

architecture rtl of frame_writer is
  signal addr1, addr2: unsigned(11 downto 0) := to_unsigned(0, 12);
  signal data: std_logic_vector(15 downto 0);
  signal we: std_logic := '0';
begin

  ram_addr <= std_logic_vector(addr2);
  ram_data <= data;
  ram_clk <= clk;
  ram_we <= we;

  process(clk)
  begin
    if rst = '1' then
      addr1 <= to_unsigned(0, 12);
    elsif rising_edge(clk) then
      we <= '0';

      if spi_data_ready = '1' then
        data <= spi_data;
        addr1 <= addr1 + 1;
        addr2 <= addr1;
        we <= '1';
      end if;

    end if;
  end process;

end rtl;
