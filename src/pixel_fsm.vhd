library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity pixel_fsm is
  port (
    pixel_clk: in std_logic;

    pixel_addr: out std_logic_vector(10 downto 0);
    pixel_pwm: out std_logic_vector(2 downto 0);

    led_row: out std_logic_vector(4 downto 0);
    led_latch: out std_logic;
    led_oe: out std_logic
  );
end pixel_fsm;

architecture rtl of pixel_fsm is
  type state_type is (s_col, s_row1, s_row2, s_pwm);
  signal state: state_type := s_col;

  signal col: std_logic_vector(5 downto 0) := "000000";
  signal row: std_logic_vector(4 downto 0) := "00000";
  signal pwm: std_logic_vector(2 downto 0) := "000";

  signal lat: std_logic := '0';
  signal oe: std_logic := '0';
begin

  led_row <= std_logic_vector(unsigned(row) - 1);
  led_latch <= lat;
  led_oe <= oe;

  pixel_addr <= row & col;
  pixel_pwm <= pwm;

  process(pixel_clk)
  begin
    if rising_edge(pixel_clk) then

      lat <= '0';
      oe <= '0';

      case state is

        when s_col =>
          col <= std_logic_vector(unsigned(col) + 1);
          if col = "111111" then -- about to overflow (switch row)
            oe <= '1';
            state <= s_row1;
          else
            state <= s_col;
          end if;

        when s_row1 =>
          lat <= '1';
          oe <= '1';
          row <= std_logic_vector(unsigned(row) + 1);
          state <= s_row2;

        when s_row2 =>
          oe <= '1';
          if row = "00000" then -- overflowed
            state <= s_pwm;
          else
            state <= s_col;
          end if;

        when s_pwm =>
          pwm <= std_logic_vector(unsigned(pwm) + 1);
          state <= s_col;

        when others =>
          state <= s_col;
      end case;

    end if;
  end process;

end rtl;
