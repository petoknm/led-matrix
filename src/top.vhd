library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top is
  port(
    clk_50m: in std_logic;

    clk_out: out std_logic;
    latch: out std_logic;
    oe: out std_logic;
    row: out std_logic_vector(4 downto 0);

    rgb1: out std_logic_vector(2 downto 0);
    rgb2: out std_logic_vector(2 downto 0);

    spi_clk: in std_logic;
    spi_mosi: in std_logic;
    spi_cs: in std_logic;

    data_ready: out std_logic;
    data_start: out std_logic
  );
end top;

architecture behavioral of top is
  signal clk_100m, clk_10m, clk_1m, clk_10m_90deg, clk_1m_90deg: std_logic;
  signal rgbrgb: std_logic_vector(5 downto 0);

  signal ram_addr: std_logic_vector(11 downto 0);
  signal ram_data, spi_data: std_logic_vector(15 downto 0);
  signal ram_we, ram_clk, spi_data_ready, spi_data_start: std_logic;
begin

  data_ready <= spi_data_ready;
  data_start <= spi_data_start;

  rgb1 <= rgbrgb(2 downto 0);
  rgb2 <= rgbrgb(5 downto 3);
  clk_out <= clk_10m_90deg;

  pll_inst: entity work.pll port map (
    inclk0 => clk_50m,
    c0	   => clk_100m,
    c1     => clk_10m,
    c2     => clk_1m,
    c3     => clk_10m_90deg,
    c4     => clk_1m_90deg
  );

  pe: entity work.pixel_engine port map (
    ram_addr => ram_addr,
    ram_data => ram_data,
    ram_we => ram_we,
    ram_clk => ram_clk,

    pixel_clk => clk_10m,

    led_data => rgbrgb,
    led_row => row,
    led_latch => latch,
    led_oe => oe
  );

  fw: entity work.frame_writer port map (
    clk => clk_10m,

    ram_addr => ram_addr,
    ram_data => ram_data,
    ram_clk => ram_clk,
    ram_we => ram_we,

    spi_data => spi_data,
    spi_data_ready => spi_data_ready,
    rst => spi_data_start
  );

  spi: entity work.spi port map (
    clk => clk_10m,

    spi_clk => spi_clk,
    spi_mosi => spi_mosi,
    spi_cs => spi_cs,

    data => spi_data,
    data_ready => spi_data_ready,
    data_start => spi_data_start
  );

end behavioral;
