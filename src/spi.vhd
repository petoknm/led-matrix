library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity spi is
  port (
    clk: in std_logic;

    spi_clk: in std_logic;
    spi_mosi: in std_logic;
    spi_cs: in std_logic;

    data: out std_logic_vector(15 downto 0);
    data_ready: out std_logic;
    data_start: out std_logic
  );
end spi;

architecture rtl of spi is
  signal clk_reg1, clk_reg2, clk_reg3: std_logic := '0';
  signal cs_reg1, cs_reg2, cs_reg3: std_logic := '1';
  signal mosi_reg1, mosi_reg2: std_logic := '0';
  signal bits: unsigned(3 downto 0) := to_unsigned(0, 4);
begin

  process(clk)
  begin
    if rising_edge(clk) then
      data_ready <= '0';
      data_start <= '0';

      clk_reg1 <= spi_clk;
      clk_reg2 <= clk_reg1;
      clk_reg3 <= clk_reg2;

      cs_reg1 <= spi_cs;
      cs_reg2 <= cs_reg1;
      cs_reg3 <= cs_reg2;

      mosi_reg1 <= spi_mosi;
      mosi_reg2 <= mosi_reg1;

      if cs_reg2 = '1' then
        bits <= to_unsigned(0, 3);
      elsif cs_reg3 = '1' and cs_reg2 = '0' then
        data_start <= '1';
      else
        if clk_reg3 = '0' and clk_reg2 = '1' then -- rising edge
          data <= data(14 downto 0) & mosi_reg2; -- push the data bit in
          bits <= bits + 1;
          if bits = 15 then
            data_ready <= '1';
          end if;
        end if;
      end if;
    end if;
  end process;

end rtl;
