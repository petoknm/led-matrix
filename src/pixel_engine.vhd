library ieee;
use ieee.std_logic_1164.all;

entity pixel_engine is
  port (
    ram_addr: in std_logic_vector(11 downto 0);
    ram_data: in std_logic_vector(15 downto 0);
    ram_we: in std_logic;
    ram_clk: in std_logic;

    pixel_clk: in std_logic;

    led_data: out std_logic_vector(5 downto 0);
    led_row: out std_logic_vector(4 downto 0);
    led_latch: out std_logic;
    led_oe: out std_logic
  );
end pixel_engine;

architecture rtl of pixel_engine is
  signal pixel_addr: std_logic_vector(10 downto 0);
  signal pixel_pwm: std_logic_vector(2 downto 0);
begin

  fsm: entity work.pixel_fsm port map (
    pixel_clk => pixel_clk,

    pixel_addr => pixel_addr,
    pixel_pwm => pixel_pwm,

    led_row => led_row,
    led_latch => led_latch,
    led_oe => led_oe
  );

  pp1: entity work.pixel_pipeline port map (
    ram_addr => ram_addr(10 downto 0),
    ram_data => ram_data,
    ram_we => (not ram_addr(11)) and ram_we,
    ram_clk => ram_clk,

    pixel_clk => pixel_clk,
    pixel_addr => pixel_addr,
    pixel_pwm => pixel_pwm,

    rgb => led_data(2 downto 0)
  );

  pp2: entity work.pixel_pipeline port map (
    ram_addr => ram_addr(10 downto 0),
    ram_data => ram_data,
    ram_we => ram_addr(11) and ram_we,
    ram_clk => ram_clk,

    pixel_clk => pixel_clk,
    pixel_addr => pixel_addr,
    pixel_pwm => pixel_pwm,

    rgb => led_data(5 downto 3)
  );

end rtl;
