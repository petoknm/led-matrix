extern crate framebuffer;
extern crate spidev;

use std::io::{BufRead, Write, stdin};
use std::fs::File;
use std::sync::{Arc, RwLock};
use std::env::var;

use framebuffer::{Framebuffer};
use spidev::{Spidev, SpidevOptions, SPI_MODE_0};

fn create_spi() -> std::io::Result<Spidev> {
    let mut spi = try!(Spidev::open(
        var("SPI").unwrap_or("/dev/spidev0.0".to_string())
    ));
    try!(spi.configure(&SpidevOptions::new()
         .bits_per_word(8)
         .max_speed_hz(5_000_000)
         .mode(SPI_MODE_0)));
    Ok(spi)
}

fn main() {
    let mut spi = create_spi().unwrap();

    let fb = Framebuffer::new(
        var("FB").unwrap_or("/dev/fb0".to_string())
    ).unwrap();

    let width = fb.var_screen_info.xres as usize;
    let height = fb.var_screen_info.yres as usize;
    let line_length = fb.fix_screen_info.line_length as usize;
    let bytespp = fb.var_screen_info.bits_per_pixel as usize / 8;

    println!("res: {}x{}", width, height);
    println!("bpp: {}", fb.var_screen_info.bits_per_pixel);
    println!("red: {:?}", fb.var_screen_info.red);
    println!("green: {:?}", fb.var_screen_info.green);
    println!("blue: {:?}", fb.var_screen_info.blue);

    let output_width = 64;
    let output_height = 64;

    let mut frame: Vec<_> = (0..output_width * output_height)
        .map(|i| if i % 2 == 1 { 0xFF } else { 0x00 })
        .collect();

    let start_pos = Arc::new(RwLock::new((0usize, 0usize)));

    let sp = start_pos.clone();
    std::thread::spawn(move || {
        let stdin = stdin();
        for l in stdin.lock().lines() {
            for c in l.unwrap().chars() {
                let mut sp = sp.write().unwrap();
                match c {
                    'w' => (*sp).1 -= 10,
                    'a' => (*sp).0 -= 10,
                    's' => (*sp).1 += 10,
                    'd' => (*sp).0 += 10,
                    _   => {},
                }
            }
        }
    });

    {
        let mut f = File::create("out.data").unwrap();
        let mut v = vec![];
        fb.read_frame(&mut v);
        f.write(&v).unwrap();
    }

    loop {
        let (start_width , start_height) = *start_pos.read().unwrap();
        println!("offset: {}x{}", start_width, start_height);

        std::thread::sleep(std::time::Duration::from_millis(100));

        for y in 0..output_height {
            let y_index = (y + start_height) * line_length;
            for x in 0..output_width {
                let index = y_index + (x + start_width) * bytespp;
                let b = fb[index + 0] >> 6;
                let g = fb[index + 1] >> 5;
                let r = fb[index + 2] >> 5;
                frame[x + y*output_width] = (r << 5) | (g << 2) | b;
            }
        }

        spi.write(&frame).unwrap();
    }
}
