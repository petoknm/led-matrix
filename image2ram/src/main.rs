extern crate image;
extern crate ihex;

use image::GenericImage;
use ihex::record::Record;
use ihex::writer;

fn main() {
    let img = image::open("ram_half.png").unwrap();

    let bytes: Vec<_> = img.pixels().map(|(_, _, pixel)| -> u8 {
        let [r, g, b, _] = pixel.data;
        let r = r >> 5;
        let g = g >> 5;
        let b = b >> 6;
        r << 5 | g << 2 | b
    }).collect();

    let chunk_size = 64;

    let mut records: Vec<_> = bytes.as_slice().chunks(chunk_size).enumerate().map(|(i, chunk)| {
        Record::Data { offset: (chunk_size * i) as u16, value: chunk.to_vec() }
    }).collect();

    records.push(Record::EndOfFile);

    let object = writer::create_object_file_representation(&records).unwrap();
    println!("{}", object);

    // println!("{}", strings.as_slice().join(", "));
}
